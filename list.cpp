#include <iostream>
#include "list.h"
using namespace std;

List::~List() 
{
	for ( Node *p; !isEmpty(); ) 
	{
		p = head->next;
		delete head;
		head = p;
	}
}



void List::headPush(int a) {//Add element to front of list
	Node *newnode = new Node(a);

	if (!isEmpty()) 
	{
		newnode->next = head;
		head = newnode;
	}
	else 
	{
		head = newnode;
		tail = newnode;
	}

}


void List::tailPush(int b) //Add element to tail of list
{
	Node *newnode = new Node(b);

	if (!isEmpty()) 
	{
		tail->next = newnode;
		tail = newnode;
	}
	else 
	{
		head = newnode;
		tail = newnode;
	}
}


int List::headPop() //Remove and return element from head of list
{
	int a = 0;
	if (head != 0 && head == tail) 
	{
		Node *x = 0;
		x = head;
		a = x->info;
		head = 0;
		tail = 0;
		return a;
	}
	else if (head != 0 && tail != 0) 
	{
		Node *x;
		x = head;
		head = head->next;
		a = x->info;
		delete x;
		return a;
	}
	return 0;
}


int List::tailPop()	//Remove and return element from tail of list
{
	Node *newnode = head;
	int a = tail->info;
	if (isEmpty()) { return NULL; }
	if (!isEmpty() && head != tail)
	{
		while (newnode->next->next != NULL)
		{
			newnode = newnode->next;
		}
		tail = newnode;
		newnode = tail->next;
		tail->next = NULL;
		delete newnode;
		return a;
	}
	else if (!isEmpty() && head == tail)
	{
		a = newnode->info;
		head = newnode->next;
		delete newnode;
		return a;
	}
	return NULL;
}

void List::deleteNode(int a)	//Delete a particular value
{
	Node *x = head, *y = head;
	while (x != 0)
	{
		if (x->info == a)
		{
			if (x == head && head != tail)
			{
				head = head->next;
				cout << a << " is deleted" << endl;
				break;
			}
			else if (head == tail)
			{
				head = 0;
				tail = 0;
				cout << a << " is deleted" << endl;
				break;
			}
			else if (x == tail)
			{
				tail = y;
				tail->next = 0;
				delete x;
				cout << a << " is deleted" << endl;
				break;
			}
			else
			{
				y->next = x->next;
				cout << a << " is deleted" << endl;
				break;
			}
		}
		else
		{
			if (x == tail)
			{
				cout << "Not Found!" << endl;
				break;
			}
			y = x;
			x = x->next;
		}
	}
}

bool List::isInList(int a)	//Check if a particular value is in the list
{
	Node *x, *y = head;
	x = head;
	while (true) 
	{
		//cout << x->info << " ";
		if (head != 0 && x->info == a) 
		{
			return true;
		}
		else if (x != tail) 
		{
			y = x;
			x = x->next;
		}
		else 
		{
			break;
		}
	}
	return false;
}


void List::Cout() {
	Node *p = head;
	while (p != NULL) {
		std::cout << p->info;
		p = p->next;
	}
}
